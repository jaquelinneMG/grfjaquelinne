/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JoglBase;

/**
 *
 * @author jaquelinne
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;        
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.*;  // GL constants
import static com.jogamp.opengl.GL2.*; // GL2 constants
import static com.jogamp.opengl.GL2ES3.GL_QUADS;
import com.jogamp.opengl.GLProfile;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * JoglBase Programa Plantilla (GLCanvas)
 */
@SuppressWarnings("serial")
public class Text extends GLCanvas implements GLEventListener, KeyListener  {
   // Define constants for the top-level container
   private static String TITLE = "Plantilla Base java open gl";  // window's title
   private static final int CANVAS_WIDTH = 640;  // width of the drawable
   private static final int CANVAS_HEIGHT = 480; // height of the drawable
   private float anglePyramid = 0;
   private static final int FPS = 24; // animator's target frames per second
   private static final float factInc = 5.0f; // animator's target frames per second
   private float fovy = 45.0f;
   
   private GLU glu;  // for the GL Utility
   private GLUT glut;
   
   //referencias de rotacion
   float rotacion=0.0f;
   float despl=0.0f;
   float despX=0.0f;
   float despZ=0.0f;
   
   // Posicion de la luz.
  float lightX=1f;
  float lightY=1f;
  float lightZ=1f;
  float dLight=0.05f;
  
  Texture gatu,pez,bote,pezz,vela,ventana,barco;

  // Material  y luces.
  final float ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
  final float position[] = { lightX, lightY, lightZ, 1.0f };
  final float mat_diffuse[] = { 0.6f, 0.6f, 0.6f, 1.0f };
  final float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  final float mat_shininess[] = { 50.0f };

  final float[] colorBlack  = {0.0f,0.0f,0.0f,1.0f};
  final float[] colorWhite  = {1.0f,1.0f,1.0f,1.0f};
  final float[] colorGray   = {0.4f,0.4f,0.4f,1.0f};
  final float[] colorDarkGray = {0.2f,0.2f,0.2f,1.0f};
  final float[] colorRed    = {1.0f,0.0f,0.0f,1.0f};
  final float[] colorGreen  = {0.0f,1.0f,0.0f,1.0f};
  final float[] colorBlue   = {0.0f,0.0f,0.6f,1.0f};
  final float[] colorYellow = {1.0f,1.0f,0.0f,1.0f};
  final float[] colorLightYellow = {.5f,.5f,0.0f,1.0f};
  final float[] colorDim = {0.6f,07f,0.9f};
  final float[] colorCiel = {0.6f,0.4f,0.6f};
  final float[] colorROp = {0.7f,0.7f,0.8f};
  final float[] colorNar = {0.9f,0.3f,0.1f};
  
    
   /** The entry main() method to setup the top-level container and animator */
   public static void main(String[] args) {
      // Run the GUI codes in the event-dispatching thread for thread safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            // Create the OpenGL rendering canvas
            GLCanvas canvas = new Text();
            canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
 
            // Create a animator that drives canvas' display() at the specified FPS.
            final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
 
            // Create the top-level container
            final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
            JPanel panel1 = new JPanel();
            
            FlowLayout fl = new FlowLayout();
            frame.setLayout(fl);
            
            panel1.add(canvas);
            frame.getContentPane().add(panel1);
            
            frame.addKeyListener((KeyListener) canvas);
            
            frame.addWindowListener(new WindowAdapter() {
               @Override
               public void windowClosing(WindowEvent e) {
                  // Use a dedicate thread to run the stop() to ensure that the
                  // animator stops before program exits.
                  new Thread() {
                     @Override
                     public void run() {
                        if (animator.isStarted()) animator.stop();
                        System.exit(0);
                     }
                  }.start();
               }
            });
            
            frame.addComponentListener(new ComponentAdapter(){
                    public void componentResized(ComponentEvent ev) {
                            Component c = (Component)ev.getSource();
                            // Get new size
                            Dimension newSize = c.getSize();                            
                            panel1.setSize(newSize);                                                        
                            canvas.setSize(newSize);                            
                    }   
            });
                        
            frame.setTitle(TITLE);
            frame.pack();
            frame.setVisible(true);
            animator.start(); // start the animation loop
         }
      });
   }
 
   /** Constructor to setup the GUI for this Component */
   public Text() {
      this.addGLEventListener(this);
      this.addKeyListener(this);
   }
 
   // ------ Implement methods declared in GLEventListener ------
 
   /**
    * Called back immediately after the OpenGL context is initialized. Can be used
    * to perform one-time initialization. Run only once.
    */
   @Override
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                        // get GL Utilities
      glut = new GLUT();
      gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // set background (clear) color
      gl.glClearDepth(1.0f);      // set clear depth value to farthest
      gl.glEnable(GL_DEPTH_TEST); // enables depth testing
      gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
      //gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting  
   
      setSomeWhiteMaterial( gl, GL.GL_COMPRESSED_TEXTURE_FORMATS );
      
      // Alguna luz de ambiente global.
      //gl.glLightModelfv( GL2.GL_LIGHT_MODEL_AMBIENT, 
			// this.ambient, 0 );
      
      // First Switch the lights on.
      gl.glEnable( GL2.GL_LIGHTING );
      //gl.glDisable( GL2.GL_LIGHTING );
      gl.glEnable( GL2.GL_LIGHT0 );
      gl.glEnable( GL2.GL_LIGHT1 );
      //gl.glEnable( GL2.GL_LIGHT2 );
      //gl.glEnable( GL2.GL_LIGHT3 ); 
      //gl.glEnable( GL2.GL_LIGHT4 ); // Posicional en Origen


      // Light 0.
      //	      
      //
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, colorBlue, 1 );
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colorBlue, 1 );
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, colorWhite, 1 );	

      // Light 1.
      //
      
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_AMBIENT, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_DIFFUSE, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_SPECULAR, colorBlue, 0 );
      //gl.glLightfv( GL.GL_LIGHT1, GL.GL_SPECULAR, colorRed, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT1, GL2.GL_CONSTANT_ATTENUATION, 0.5f );

      // Light 2.
      //
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_AMBIENT, colorDim, 0 );
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_DIFFUSE, colorNar, 0 );
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_SPECULAR, colorNar, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT2, GL2.GL_CONSTANT_ATTENUATION, 0.8f );

      // Light 3.
      //
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_AMBIENT, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_DIFFUSE, colorDim, 0 );
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_SPECULAR, colorWhite, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT3, GL2.GL_CONSTANT_ATTENUATION, 0.3f );

      // Light 4.
      //
      //gl.glLightfv( GL.GL_LIGHT4, GL.GL_AMBIENT, colorWhite, 0 );
      //gl.glLightfv( GL.GL_LIGHT4, GL.GL_DIFFUSE, colorWhite, 0 );
      //gl.glLightfv( GL2.GL_LIGHT4, GL2.GL_SPECULAR, colorWhite, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT4, GL2.GL_CONSTANT_ATTENUATION, 0.3f );

        this.gatu = this.loadTexture("src/Imagen/gatu.jpg");
        this.barco = this.loadTexture("src/Imagen/barco.jpg");
        this.bote  = this.loadTexture("src/Imagen/bote.jpg");
        this.pez = this.loadTexture("src/Imagen/pez.png");
        this.pezz = this.loadTexture("src/Imagen/pezz.png");
        this.ventana = this.loadTexture("src/Imagen/ventana.jpg");
        this.vela = this.loadTexture("src/Imagen/vela.jpg");
        
        
        // Habilitar el uso de texturas
        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glEnable(GL2.GL_BLEND);
      
   }
   
   
    Texture loadTexture(String imageFile) {
        Texture text1 = null;
        try {
            BufferedImage buffImage = ImageIO.read(new File(imageFile));
            text1 = AWTTextureIO.newTexture(GLProfile.getDefault(), buffImage, false);
        } catch (IOException ioe) {
            System.out.println("Problema al cargar el archivo " + imageFile);
        }
        return text1;
    }
 
   /////////////// Define Material /////////////////////

  public void setLightSphereMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorYellow, 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorYellow, 0 );
      //gl.glMaterialfv( face, GL.GL_EMISSION, colorLightYellow , 0 );
      //gl.glMaterialfv( face, GL.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeMaterial( GL2 gl, int face, float rgba[], int offset )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_SHININESS, rgba, offset );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, mat_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, mat_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_SHININESS, mat_shininess, 0 );
    }

  public void setSomeWhiteMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorWhite , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorWhite , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorWhite , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeGrayMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorGray , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorGray , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeDarkGrayMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorDarkGray , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }


  public void setSomeYellowMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorBlack , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorLightYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorYellow , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 5 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeBlueMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorBlue , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorBlue, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorBlue , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeRedMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorRed , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorRed , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorRed , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeGreenMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorGreen , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorGreen , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 10 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorDarkGray , 0 );
      
    }
   
   /**
    * Call-back handler for window re-size event. Also called when the drawable is
    * first set to visible.
    */
   @Override
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
 
      if (height == 0) height = 1;   // prevent divide by zero
      float aspect = (float)width / height;
 
      // Set the view port (display area) to cover the entire window
      gl.glViewport(0, 0, width, height);

      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      glu.gluPerspective(fovy, aspect, 0.1, 50.0); // fovy, aspect, zNear, zFar
      
      /*
      // Enable the model-view transform
      gl.glMatrixMode(GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
      */
   }
 
   /**
    * Called back by the animator to perform rendering.
    */
   @Override
   public void display(GLAutoDrawable drawable) {
       
       GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();  // reset the model-view matrix
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        gl.glClearColor(0.6f, 0.8f, 0.9f,0.0f);
        
        gl.glLoadIdentity();
        
        glu.gluLookAt(2.0f, 2.0f, 8.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);  
        
        //gl.glColor3f(0.0f,0.0f,1.0f);
        
        gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(-100.0f,0.0f,0.0f);
            gl.glVertex3f(100.0f,0.0f,0.0f);
        gl.glEnd();

        gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(0.0f,-100.0f,0.0f);
            gl.glVertex3f(0.0f,100.0f,0.0f);
        gl.glEnd();

        gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(0.0f,0.0f,-100.0f);
            gl.glVertex3f(0.0f,0.0f,100.0f);
        gl.glEnd();
        
        gl.glLoadIdentity();
        
        glu.gluLookAt(2.0f, 2.0f, 8.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        gl.glTranslatef(this.despX,0.0f,this.despZ);
        
        //gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
       
        gl.glEnd();
         
        
         //gl.glTexCoord2f(0, 1f);//im1
       //gl.glTexCoord2f(1f, 1f);//im2
       //gl.glTexCoord2f(1f, 0);//im3
       //gl.glTexCoord2f(0, 0);//im4  
        this.bote.bind(gl);
        this.bote.enable(gl);
       gl.glBegin(GL2.GL_QUADS);
       
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-1.3f,0f,.8f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (1.3f,0f,.8f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.8f, -.4f,.8f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-.8f,-.4f,.8f); //4
      
      gl.glVertex3f (1.3f,0f,0f); //2
      gl.glVertex3f (.8f, -.4f,0f); //3
      gl.glVertex3f (-.8f,-.4f,0f); //4
      gl.glVertex3f (-1.3f,0f,0f); //1
      
      gl.glVertex3f (-1.3f,0f,0f); //1
      gl.glVertex3f (-1.3f,0f,.8f); //1
      gl.glVertex3f (1.3f,0f,.8f); //2
      gl.glVertex3f (1.3f,0f,0f); //2
      
      gl.glVertex3f (1.3f,0f,.8f); //2
      gl.glVertex3f (1.3f,0f,0f); //2
      gl.glVertex3f (.8f, -.4f,0f); //3
      gl.glVertex3f (.8f, -.4f,.8f); //3
      
      gl.glVertex3f (.8f, -.4f,0f); //3
      gl.glVertex3f (.8f, -.4f,.5f); //3
      gl.glVertex3f (-.8f,-.4f,.5f); //4
      gl.glVertex3f (-.8f,-.4f,0f); //4
      
      gl.glVertex3f (-.8f,-.4f,.8f); //4
      gl.glVertex3f (-.8f,-.4f,0f); //4
      gl.glVertex3f (-1.3f,0f,0f); //1
      gl.glVertex3f (-1.3f,0f,.8f); //1
      gl.glEnd();
      //bote.disable(gl);
      
      
      this.vela.bind(gl);
       this.vela.enable(gl);
       
      gl.glBegin(GL2.GL_QUADS);
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (.1f,.1f,.6f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (.1f,2.3f,.6f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.8f, .1f,.6f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (.1f,.1f,.6f); //1
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (.1f,.1f,.3f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (.1f,2.3f,.3f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.8f, .1f,.3f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (.1f,.1f,.3f); //1
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (.1f,.1f,.3f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (.1f,.1f,.6f); //1
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.1f,2.3f,.6f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.1f,2.3f,.3f); //2
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (.1f,2.3f,.6f); //2
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (.1f,2.3f,.3f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.8f, .1f,.3f); //3
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (.8f, .1f,.6f); //3
      
      //2a vela
      gl.glTexCoord2f(0, 0);//im4
       gl.glVertex3f (-.1f,.1f,.6f); //1
       gl.glTexCoord2f(1f, 0);//im3
       gl.glVertex3f (-.1f,2.5f,.6f); //2
       gl.glTexCoord2f(1f, 1f);//im2
       gl.glVertex3f (-.8f, .1f,.6f); //3
       gl.glTexCoord2f(0, 1f);//im1
       gl.glVertex3f (-.1f,.1f,.6f); //1
       
       gl.glTexCoord2f(0, 0);//im4
       gl.glVertex3f (-.1f,.1f,.3f); //1
       gl.glTexCoord2f(1f, 0);//im3
       gl.glVertex3f (-.1f,2.5f,.3f); //2
       gl.glTexCoord2f(1f, 1f);//im2
       gl.glVertex3f (-.8f, .1f,.3f); //3
       gl.glTexCoord2f(0, 1f);//im1
       gl.glVertex3f (-.1f,.1f,.3f); //1
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-.1f,.1f,.3f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (-.1f,.1f,.6f); //1
      gl.glTexCoord2f(1f, 1f);//im2
       gl.glVertex3f (-.1f,2.5f,.6f); //2
       gl.glTexCoord2f(0, 1f);//im1
       gl.glVertex3f (-.1f,2.5f,.3f); //2
       
       gl.glTexCoord2f(0, 0);//im4
       gl.glVertex3f (-.1f,2.5f,.6f); //2
       gl.glTexCoord2f(1f, 0);//im3
       gl.glVertex3f (-.1f,2.5f,.3f); //2
       gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (-.8f, .1f,.3f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-.8f, .1f,.6f); //3
      
      gl.glEnd();    
            
         this.barco.bind(gl);
        this.barco.enable(gl);
       gl.glBegin(GL2.GL_QUADS);
       
            //Barco
            //gl.glTexCoord2f(0, 1f);//im1
       //gl.glTexCoord2f(1f, 1f);//im2
       //gl.glTexCoord2f(1f, 0);//im3
       //gl.glTexCoord2f(0, 0);//im4
          //base
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-7.5f,.5f,-3.5f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (-2.5f,.5f,-3.5f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (-2.8f, -.8f,-3.5f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-7.2f,-.8f,-3.5f); //4
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-7.5f,.5f,-4.5f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (-2.5f,.5f,-4.5f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (-2.8f, -.8f,-4.5f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-7.2f,-.8f,-4.5f); //4
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-7.5f,.5f,-4.5f); //1
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (-7.5f,.5f,-3.5f); //1
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (-2.5f,.5f,-3.5f); //2
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-2.5f,.5f,-4.5f); //2
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-2.5f,.5f,-3.5f); //2
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (-2.5f,.5f,-4.5f); //2
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (-2.8f, -.8f,-4.5f); //3
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-2.8f, -.8f,-3.5f); //3
      
      gl.glTexCoord2f(0, 0);//im4
      gl.glVertex3f (-2.8f, -.8f,-4.5f); //3
      gl.glTexCoord2f(1f, 0);//im3
      gl.glVertex3f (-2.8f, -.8f,-3.5f); //3
      gl.glTexCoord2f(1f, 1f);//im2
      gl.glVertex3f (-7.2f,-.8f,-3.5f); //4
      gl.glTexCoord2f(0, 1f);//im1
      gl.glVertex3f (-7.2f,-.8f,-4.5f); //4
      
      gl.glEnd();  
      
           
            gl.glBegin(GL2.GL_QUADS);
      //cubierta
      gl.glVertex3f (-6.7f,.5f,-3.5f); //1
      gl.glVertex3f (-3.2f,.5f,-3.5f); //2
      gl.glVertex3f (-3.2f,0.8f,-3.5f); //3
      gl.glVertex3f (-6.7f,0.8f,-3.5f); //3
      
      gl.glVertex3f (-5.9f,0.8f,-3.5f); //1
      gl.glVertex3f (-5.5f,0.8f,-3.5f); //1
      gl.glVertex3f (-5.5f,1.2f,-3.5f); //1
      gl.glVertex3f (-5.9f,1.2f,-3.5f); //1
      
      
      
      
      gl.glVertex3f (-6.7f,.5f,-4.5f); //1
      gl.glVertex3f (-3.2f,.5f,-4.5f); //2
      gl.glVertex3f (-3.2f,0.8f,-4.5f); //3
      gl.glVertex3f (-6.7f,0.8f,-4.5f); //3
      
      gl.glVertex3f (-5.9f,0.8f,-4.5f); //1
      gl.glVertex3f (-5.5f,0.8f,-4.5f); //2
      gl.glVertex3f (-5.5f,1.2f,-4.5f); //3
      gl.glVertex3f (-5.9f,1.2f,-4.5f); //4
      
      gl.glVertex3f (-5.9f,0.8f,-4.5f); //1
      gl.glVertex3f (-5.9f,0.8f,-3.5f); //1
      gl.glVertex3f (-5.5f,0.8f,-3.5f); //2
      gl.glVertex3f (-5.5f,0.8f,-4.5f); //2
      
      gl.glVertex3f (-5.5f,0.8f,-3.5f); //2
      gl.glVertex3f (-5.5f,0.8f,-4.5f); //2
      gl.glVertex3f (-5.5f,1.2f,-4.5f); //3
      gl.glVertex3f (-5.5f,1.2f,-3.5f); //3
      
      gl.glVertex3f (-5.5f,1.2f,-4.5f); //3
      gl.glVertex3f (-5.5f,1.2f,-3.5f); //3
      gl.glVertex3f (-5.9f,1.2f,-3.5f); //4
      gl.glVertex3f (-5.9f,1.2f,-4.5f); //4
      
      
      gl.glVertex3f (-6.7f,.5f,-4.5f); //1
      gl.glVertex3f (-6.7f,.5f,-3.5f); //1
      gl.glVertex3f (-3.2f,.5f,-3.5f); //2
      gl.glVertex3f (-3.2f,.5f,-4.5f); //2
      
      gl.glVertex3f (-3.2f,.5f,-4.5f); //2
      gl.glVertex3f (-3.2f,.5f,-3.5f); //2
      gl.glVertex3f (-3.2f,0.8f,-3.5f); //3
      gl.glVertex3f (-3.2f,0.8f,-4.5f); //3
      
      gl.glVertex3f (-3.2f,0.8f,-3.5f); //3
      gl.glVertex3f (-3.2f,0.8f,-4.5f); //3
      gl.glVertex3f (-6.7f,0.8f,-4.5f); //3
      gl.glVertex3f (-6.7f,0.8f,-3.5f); //3
      
      
      
      
          gl.glEnd();  
      
            this.ventana.bind(gl);
        this.ventana.enable(gl);
            gl.glBegin(GL2.GL_QUADS);  
           //ventana
           // gl.glColor3f (0.2f,0.5f,0.4f); // pezclr
           //gl.glTexCoord2f(0, 1f);//im1
       //gl.glTexCoord2f(1f, 1f);//im2
       //gl.glTexCoord2f(1f, 0);//im3
       //gl.glTexCoord2f(0, 0);//im4
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-3.59f,-.25f,-3.5f);//1
            gl.glTexCoord2f(0f, 0);//im3            
            gl.glVertex3f(-3.45f, -.35f,-3.5f);//2 
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-3.15f, -.35f,-3.5f);//3
            gl.glTexCoord2f(0, 0f);//im1
            gl.glVertex3f(-3.05f,-.25f, -3.5f);//4
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-3.05f,.1f, -3.5f);//5
            gl.glTexCoord2f(0f, 0);//im3
            gl.glVertex3f(-3.15f,.2f, -3.5f);//6
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-3.45f,.2f, -3.5f);//7
            gl.glTexCoord2f(0, 0f);//im1
            gl.glVertex3f(-3.59f,.1f, -3.5f);//8

            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-3.05f,-.25f, -3.5f);//4
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(-3.05f,.1f, -3.5f);//5
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(-3.59f,.1f, -3.5f);//8
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(-3.59f,-.25f,-3.5f);//1
            
            //ventana2
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-4.64f,-.25f,-3.5f);//1 
            gl.glTexCoord2f(0f, 0);//im3
            gl.glVertex3f(-4.5f, -.35f,-3.5f);//2 
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-4.2f, -.35f,-3.5f);//3
            gl.glTexCoord2f(0, 0f);//im1
            gl.glVertex3f(-4.1f,-.25f, -3.5f);//4
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-4.1f,.1f, -3.5f);//5
            gl.glTexCoord2f(0f, 0);//im3
            gl.glVertex3f(-4.2f,.2f, -3.5f);//6
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-4.5f,.2f, -3.5f);//7
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-4.64f,.1f, -3.5f);//8

            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-4.1f,-.25f, -3.5f);//4
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(-4.1f,.1f, -3.5f);//5
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(-4.64f,.1f, -3.5f);//8
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(-4.64f,-.25f,-3.5f);//1
            
            //ventana3
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-5.64f,-.25f,-3.5f);//1 
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.5f, -.35f,-3.5f);//2 
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.2f, -.35f,-3.5f);//3
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.1f,-.25f, -3.5f);//4
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.1f,.1f, -3.5f);//5
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.2f,.2f, -3.5f);//6
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.5f,.2f, -3.5f);//7
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-5.64f,.1f, -3.5f);//8

            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-5.1f,-.25f, -3.5f);//4
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(-5.1f,.1f, -3.5f);//5
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(-5.64f,.1f, -3.5f);//8
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(-5.64f,-.25f,-3.5f);//1
            
            //ventana4
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-6.64f,-.25f,-3.5f);//1  
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-6.5f, -.35f,-3.5f);//2
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-6.2f, -.35f,-3.5f);//3
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-6.1f,-.25f, -3.5f);//4
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-6.1f,.1f, -3.5f);//5
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-6.2f,.2f, -3.5f);//6
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-6.5f,.2f, -3.5f);//7
            gl.glTexCoord2f(0f, 0f);//im2
            gl.glVertex3f(-6.64f,.1f, -3.5f);//8

            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-6.1f,-.25f, -3.5f);//4
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(-6.1f,.1f, -3.5f);//5
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(-6.64f,.1f, -3.5f);//8
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(-6.64f,-.25f,-3.5f);//1
            gl.glEnd();
            
            gl.glRotatef(this.rotacion, .0f, -1.5f, .0f);
           this.pez.bind(gl);
           this.pez.enable(gl);
            //cara
            gl.glBegin(GL2.GL_QUADS);
            //gl.glColor3f (0.2f,0.5f,0.4f); // pez
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(2.0f, -1.0f, 0f); //1
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(2.5f, -1.0f, 0f); //2  
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(2.5f, -1.5f, 0f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.5f, 0f);  //4
        
             gl.glEnd();  
      
           this.pezz.bind(gl);
           this.pezz.enable(gl);
            gl.glBegin(GL2.GL_QUADS);
            
            //cuerpo
            
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(2.0f, -1.0f, -.5f);//1
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(2.5f, -1.0f, -.5f);//2 
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(2.5f, -1.5f, -.5f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.5f, -.5f);//4
            
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.0f, -.5f);//1
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.0f, 0.0f);//1
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.5f, -1.0f, 0.0f);//2
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.5f, -1.0f, -.5f);//2
            
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.5f, -1.0f, 0.0f);//2
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.5f, -1.0f, -.5f);//2
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.5f, -1.5f, -.5f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.5f, -1.5f, 0.0f);//3
            
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.5f, 0.0f);//4
            gl.glTexCoord2f(0, 1f);//im1            
            gl.glVertex3f(2.5f, -1.5f, 0.0f);//3
            gl.glTexCoord2f(0, 1f);//im1            
            gl.glVertex3f(2.5f, -1.5f, -.5f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.5f, -.5f);//4
           
            
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.5f, -.5f);//4
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(2.0f, -1.5f, 0f);//4
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(2.0f, -1.0f, 0f);//1
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(2.0f, -1.0f, -.5f);//1
            
            //gl.glColor3f (0.9f,0.3f,0.1f); // cola pez
            gl.glEnd();  
      
           
       gl.glBegin(GL2.GL_QUADS);
       //gl.glColor3f (0.9f,0.3f,0.1f); // cola pez
            gl.glVertex3f(2.3f, -1.0f, -.5f);//1
            gl.glVertex3f(2.3f, -1.3f, -.8f);//1
            gl.glVertex3f(2.3f, -1.0f, -.8f);//4
            gl.glVertex3f(2.3f, -1.3f, -.5f);//4
            gl.glEnd(); 
            
        this.rotacion+=5.0f;
        if (this.rotacion>360){
            this.rotacion = 0;
        }
        
        System.out.printf("Rotacion %f \n", this.rotacion);
        gl.glFlush();   
   }

   

   /**
    * Called back before the OpenGL context is destroyed. Release resource such as buffers.
    */
   @Override
   public void dispose(GLAutoDrawable drawable) { }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int codigo = e.getKeyCode();
        
        System.out.println("codigo presionado = "+codigo);
        
        switch (codigo){                     
            case KeyEvent.VK_LEFT:
                 this.despX-=0.2f;
                 break;
            case KeyEvent.VK_RIGHT:
                 this.despX+=0.2f;
                 break;  
            case KeyEvent.VK_DOWN:    
                 this.despZ+=0.2f;
                 break;                 
            case KeyEvent.VK_UP:
                 this.despZ-=0.2f;
                 break;
            case KeyEvent.VK_R:
                 this.rotacion+=5.0f;
                 break;                 
        }
        System.out.println("despX ="+this.despX+" - "+"despZ ="+this.despZ); 
    }
    @Override
    public void keyReleased(KeyEvent e) {
        
    }
}